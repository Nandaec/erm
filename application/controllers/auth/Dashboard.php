<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }
    
	public function index()
	{
        // $data['title']       = 'Login';
        // $data['description'] = 'Halaman Login';
        // $data['keywords']    = 'Login';
        // $data['page']        = 'login';
        // $this->load->view('index_blank', $data);
        $header = [
        	'title' 	=> 'RSUI BOYOLALI',
        	'file_css'	=> ''
        ];
        $body = [
        	'page' 		=> 'auth/content',
        	'user' 		=> $this->session->userdata()
        ];
        $footer = [
        	'file_js'	=> 'auth/dashboard.js'
        ];

        $this->load->view('auth/index', $body);
	}
}
