<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['form_validation', 'akses']);
        $this->akses->cek_login();
    }
	public function index()
	{
        $header = ['title' => 'LOGIN', 'file_css' => 'auth/login.css'];

        $this->form_validation->set_rules('username', 'Username', 'trim|required', ['required' => 'Masukan username dengan benar']);
        $this->form_validation->set_rules('password', 'Password', 'trim|required', ['required' => 'Masukan password']);
        if ($this->form_validation->run() === false) {
            $this->load->view('auth/login', $header);
        } else {
            $this->login();
        }
    }

    private function login(){
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

    	$dataUser = $this->main_model->Get_rows('user', array('username' => $username));
    	if ($dataUser) {
    		if ($password == $dataUser->password) {
    			$set_sesi = [
	    			'nama' 		=> $dataUser->nama,
	    			'username' 	=> $dataUser->username,
	    			'hak_akses' => $dataUser->hak_akses,
    			];
    			$this->session->set_userdata($set_sesi);

    			if (isset($set_sesi['hak_akses'])) {
    				redirect('auth/dashboard');
    			}
    		}else{
    			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <strong>Password salah. </strong></div>');
    			redirect('auth/login');
    		}
    	}else{
    		$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <strong>Username belum terdaftar. </strong></div>');
    		redirect('auth/login');
    	}
    }

    public function logout()
    {

        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('hak_akses');

        $this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert"> Berhasil keluar...</div>');
        redirect('auth/login');
    }
}
