<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gentelella Alela! | </title>

        <link href="<?=base_url('/assets/backend/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?=base_url('/assets/backend/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
        <link href="<?=base_url('/assets/backend/vendors/nprogress/nprogress.css')?>" rel="stylesheet">
        <link href="<?=base_url('/assets/backend/vendors/animate.css/animate.min.css')?>" rel="stylesheet">

        <link href="<?=base_url('/assets/backend/build/css/custom.min.css')?>" rel="stylesheet">
    </head>

    <body class="login">
        <div>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <?=$this->session->flashdata('pesan')?>
                    <input type="hidden" id="pesan" value="<?$this->session->flashdata('pesan')?>">
                    <section class="login_content">
                        <form method="post" action="<?=base_url('auth/login')?>" id="form">
                            <h1>Login Form</h1>
                            <div>
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username"/>
                            </div>
                            <div>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                            </div>
                            <div>
                                <button class="form-control btn btn-default submit" href="auth/login">Log in</button>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>