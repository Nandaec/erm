<?php
class Main_model extends CI_Model
{
    protected $bill;
    
    function __construct()
    {
        parent::__construct();
    }

    function Get_all($table)
    {
    	return $this->db->get($table)->result();
    }

    function Get_where($table,$data)
    {
    	return $this->db->get_where($table,$data)->result();
    }

    function Get_rows($table,$data)
    {
    	return $this->db->get_where($table,$data)->row();
    }
}